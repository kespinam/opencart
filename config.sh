#!/bin/bash
chmod -R 0777 system/storage/cache/
chmod 0777 system/storage/download/
chmod -R 0777 system/storage/logs/
chmod 0777 system/storage/modification/
chmod 0777 system/storage/session/
chmod 0777 system/storage/upload/
chmod 0777 system/storage/vendor/
chmod -R 0777 image/
chmod -R 0777 image/cache/
chmod -R 0777 image/catalog/
chmod 0777 config.php
chmod 0777 admin/config.php

chcon -R -t httpd_sys_script_rw_t system/storage/cache/
chcon -R -t httpd_sys_script_rw_t system/storage/download/
chcon -R -t httpd_sys_script_rw_t system/storage/logs/
chcon -R -t httpd_sys_script_rw_t system/storage/modification/
chcon -R -t httpd_sys_script_rw_t system/storage/session/
chcon -R -t httpd_sys_script_rw_t system/storage/upload/
chcon -R -t httpd_sys_script_rw_t system/storage/vendor/
chcon -R -t httpd_sys_script_rw_t image/
chcon -R -t httpd_sys_script_rw_t image/cache/
chcon -R -t httpd_sys_script_rw_t image/catalog/
chcon -R -t httpd_sys_script_rw_t config.php
chcon -R -t httpd_sys_script_rw_t admin/config.php