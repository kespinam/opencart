<?php

//constantes
define('HTTP_IP','http://cibercity.info');
define('HTTPS_IP','http://cibercity.info');

// HTTP
define('HTTP_SERVER', HTTP_IP . '/asc-opencart/admin/');
define('HTTP_CATALOG', HTTP_IP . '/asc-opencart/');

// HTTPS
define('HTTPS_SERVER', HTTP_IP  . '/asc-opencart/admin/');
define('HTTPS_CATALOG', HTTP_IP . '/asc-opencart/');

// DIR
define('DIR_APPLICATION', '/home/eq9g96i9ql8y/public_html/asc-opencart/admin/');
define('DIR_SYSTEM', '/home/eq9g96i9ql8y/public_html/asc-opencart/system/');
define('DIR_IMAGE', '/home/eq9g96i9ql8y/public_html/asc-opencart/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_CATALOG', '/home/eq9g96i9ql8y/public_html/asc-opencart/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'city');
define('DB_PASSWORD', 'oA6d5fGBk7JZWF4WI4p4');
define('DB_DATABASE', 'asc_opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
